# Awalé - JS
Projet de jeu awalé en JS
Binome - Sergent Vianney et Marius Pettex

## Tâches demandées
* Réalisation d'une wireframe
* Développer un jeu awalé en JS (tour par tour pour 2 joueurs)

## Tâches optionnelles
* Scoring
* IA pour jouer solo

## Méthode
JavaScript pour créer la logique du jeu et gérer les interactions

CSS pour gérer l'apparence

HTML pour définir la structure
    tags id et classes pour faire en sorte que les éléments soient facilement accessibles par le DOM


## UX
L'utilisateur arrive sur une page blanche, avec un seul bouton (ou un menu?) qui lorsque cliqué, affiche le tableau de jeu et permet de commencer la partie

Le jeu se joue sur un plateau constitué de deux rangées de six trous, l'une appartenant au premier joueur, l'autre au second.
Chaque rangée sera colorée de couleur distinctives afin quel'utilisateur sache quelle est la sienne.
Chaque trou affichera un nombre entre 0 et 25, qui indiquera le nombre de graines dans chaque trou.
Le joueur 1 (couleur claire) joue en premier. Il cliquera sur un de ses trous, et "plantera" les graines counterclockwise
